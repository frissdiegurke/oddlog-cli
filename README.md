# oddlog CLI

[![License](https://img.shields.io/npm/l/oddlog-cli.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/oddlog-cli.svg)](https://www.npmjs.com/package/oddlog-cli)
[![build status](https://gitlab.com/frissdiegurke/oddlog-cli/badges/master/build.svg)](https://gitlab.com/frissdiegurke/oddlog-cli/commits/master)
[![Downloads](https://img.shields.io/npm/dm/oddlog-cli.svg)](https://www.npmjs.com/package/oddlog-cli)

This is the command-line interface for [`oddlog`](https://gitlab.com/frissdiegurke/oddlog).

## Preview

[![Preview 01](https://gitlab.com/frissdiegurke/oddlog/raw/master/assets/readme-01.png)](https://gitlab.com/frissdiegurke/oddlog/raw/master/assets/readme-01.png)

## Usage

<!-- [HELP GENERATED] -->
```
Usage: oddlog [options]
oddlog reads from stdin and outputs to stdout.

Message filter options:
  -l, --level                Filter messages with given level and above.[string]
  -a, --application, --name  Filter messages by application name. The ns-matcher
                             package is used for matching; See
                             https://www.npmjs.com/package/ns-matcher#examples .
                             This allows advanced and multiple (ordered) glob
                             patterns. This option can also be set via the
                             environment variable DEBUG.                 [array]
  -d, --date                 Filter messages by date or time. The accepted
                             format is based on the ISO format
                             YYYY-MM-DDTHH:mm:ss.sTZD. Parts of this can be
                             omitted. The T can be replaced with whitespace as
                             well. If no TZD is provided, local timezone will be
                             used. Valid examples include '16-1-1' (anything on
                             the 1st January of **16), '1 12Z' (anything on the
                             1st of any month at [12:00,13:00) in UTC) and
                             'T12:0+2:4' (anything on any day at [12:00,13:00)
                             in timezone +02:04).                       [string]

Output options:
  -e, --encoding                 Set encoding format.          [default: "utf8"]
  -v, --verbose                  Increase verbosity for all levels that ain't
                                 overwritten by level specific verbosity
                                 options. This is equal to set verbosity-silent,
                                 but with counted value.                 [count]
  -i, --invalid                  Pass lines that cannot be interpreted as oddlog
                                 messages to stdout.                   [boolean]
  -o, --format                   Select a pre-defined format.
                          [choices: "csv", "json", "pretty"] [default: "pretty"]
  -s, --style                    The style variant for the format.
                                [choices: "default", "module", "decent", "none"]
  --iso-date                     Use the ISO date format.              [boolean]
  -n, --newline                  Separate records with an empty line.  [boolean]
  --LS, --line-separator, --EOL  Specify a line-separator.           [default: EOL]

CSV format options:
  --FS, --field-separator  Specify a field-separator for csv output
                                                                  [default: ","]

Level specific verbosity options:
  -S, --verbosity-silent   Set the verbosity of the level SILENT and above.
                                                                        [number]
  -T, --verbosity-trace    Set the verbosity of the level TRACE and above.
                                                                        [number]
  -D, --verbosity-debug    Set the verbosity of the level DEBUG and above.
                                                                        [number]
  -V, --verbosity-verbose  Set the verbosity of the level VERBOSE and above.
                                                                        [number]
  -I, --verbosity-info     Set the verbosity of the level INFO and above.
                                                                        [number]
  -W, --verbosity-warn     Set the verbosity of the level WARN and above.
                                                                        [number]
  -E, --verbosity-error    Set the verbosity of the level ERROR and above.
                                                                        [number]
  -F, --verbosity-fatal    Set the verbosity of the level FATAL and above.
                                                                        [number]

Options:
  -m, --module  Use a node.js module to modify behavior.                [string]
  -h, --help    Show this help message.                                [boolean]
  --config      Path to JSON config file
  --version     Show version number                                    [boolean]

Examples:
  tail -f logs/my-logs.log | oddlog         Output formatted logs as they get
                                            written.
  cat log | oddlog -a 'myApp/*'             Filter for messages with logger name
  '!myApp/void'                             prefix myApp/ but not myApp/void.
  cat log | oddlog -d                       Filter for messages with exact iso
  '2016-12-14T16:20:45.895Z' -vvvvv         date match. Display with highest
                                            verbosity
  cat log | oddlog -d '12-14 16:20'         Filter for messages within the
                                            minute of 16:20 on the 14th of
                                            december of any year.
  cat log | oddlog -d '03:21:45.895'        Filter for messages with exact local
                                            time match.
```
<!-- [/HELP GENERATED] -->

## License

The source code is licensed under [MIT](https://gitlab.com/frissdiegurke/oddlog-cli/blob/master/LICENSE). If you don't
agree to the license, you may not contribute to the project. Feel free to fork and maintain your custom build thought.
