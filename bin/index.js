#!/usr/bin/env node
"use strict";

const os = require("os");
const yargs = require("yargs");

const argumentOptions = require("../lib/constants/argument-options");
const {create: createMessage} = require("../lib/services/message");
const {fromArguments: optionsFromArguments} = require("../lib/services/options");
const format = require("../lib/services/message-operations/format");
const output = require("../lib/services/message-operations/output");
const prepare = require("../lib/services/message-operations/prepare");
const show = require("../lib/services/message-operations/show");

const outStream = process.stdout, inStream = process.stdin;

/*================================================ Initial Execution  ================================================*/

process.on("SIGINT", () => {});
process.on("SIGQUIT", () => process.exit(1));
process.on("SIGTERM", () => process.exit(1));
process.on("SIGHUP", () => process.exit(1));

read(inStream, getOptions());

/*==================================================== Functions  ====================================================*/

function getOptions() {
  let args = yargs
      .usage("Usage: $0 [options]" + os.EOL + "oddlog reads from stdin and outputs to stdout.")
      .env("ODDLOG")
      .options(argumentOptions.commands)
      .help("h")
      .alias("h", "help")
      .config()
      .version()
      .describe("h", "Show this help message.");
  for (let group of argumentOptions.groups) { args.group(group.commands, group.describe); }
  for (let example of argumentOptions.examples) { args.example(example.command, example.describe); }
  try {
    return optionsFromArguments(args.argv);
  } catch(e) {
    process.stderr.write(e.name + ": " + e.message);
    process.exit(1);
  }
}

function read(stream, options) {
  let cache = "";

  stream.setEncoding(options.encoding);

  stream.on("data", (chunk) => {
    let lines = chunk.split(options.EOL), _len = lines.length - 1;
    lines[0] = cache + lines[0];
    for (let i = 0; i < _len; i++) { processLine(lines[i], options); }
    cache = lines[_len];
  });

  stream.on("end", () => {
    if (cache) { processLine(cache, options); }
    cache = "";
  });

  stream.on("error", (err) => {
    process.stderr.write(err.stack);
    process.exit(2);
  });

  stream.resume();
}

function processLine(line, options) {
  let message;
  try {
    message = createMessage(JSON.parse(line), options);
  } catch (err) {
    if (!(err instanceof TypeError) && !(err instanceof SyntaxError)) { throw err; }
    if (options.showInvalid) { outStream.write(line + options.EOL); }
    return;
  }
  let mod = options.userModule;
  if ((mod && mod.show || show)(message, options)) {
    message = (mod && mod.prepare || prepare)(message, options);
    message = (mod && mod.format || format)(message, options);
    (mod && mod.output || output)(outStream, message, options);
    if (options.newline) { outStream.write(options.EOL); }
  }
}
