#!/usr/bin/env bash

DEST_DIR="$(cd "$(dirname "$0")/.." && pwd)"
BASE_DIR="$(dirname "$DEST_DIR")"

mkdir -p "$DEST_DIR"

test -d "$DEST_DIR/lib" && rm -r "$DEST_DIR/lib"
test -d "$DEST_DIR/bin" && rm -r "$DEST_DIR/bin"

"$BASE_DIR/node_modules/.bin/babel" --presets es2015 -d "$DEST_DIR/lib" "$BASE_DIR/lib"
code=$?

if [ ${code} -ne 0 ]; then
  echo "Babel lib transpiler failed."
  exit ${code};
fi

"$BASE_DIR/node_modules/.bin/babel" --presets es2015 -d "$DEST_DIR/bin" "$BASE_DIR/bin"
code=$?

if [ ${code} -ne 0 ]; then
  echo "Babel bin transpiler failed."
  exit ${code};
fi

cp "$BASE_DIR/LICENSE" "$DEST_DIR/"

/usr/bin/env node "$DEST_DIR"/.build/prepare_package.js
code=$?

if [ ${code} -ne 0 ]; then
  echo "Building package failed."
  exit ${code};
fi
