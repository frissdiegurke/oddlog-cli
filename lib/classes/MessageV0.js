"use strict";

const {getLevelName, getPriorName} = require("../services/levels");

const SCHEMA_VERSION = 0;

/*===================================================== Classes  =====================================================*/

/**
 * [
 *   {Number}  SCHEMA_VERSION,
 *   {?Object} meta,       // may contain meta information like process ID, hostname and platform information;
 *                         // stringified JSON object
 *   {String}  loggerName, // the name of the application (logger name)
 *   {String}  ISODate,    // the ISO formatted date string of the message
 *   {Number}  level,      // the message level
 *   {?Array}  source: [   // debugging information
 *     {String} file,      // source file that invoked the message logging method
 *     {Number} row,       // row within the file
 *     {Number} col        // col within the file
 *   ],
 *   {?String} message,    // the message text
 *   {?Object} [payload]   // the message payload as stringified JSON object
 * ]
 */
class MessageV0 {
  constructor(version, array, options) {
    const level = array[4];

    this.version = version;
    this._verboseObject = null;

    this.meta = array[1];
    this.name = array[2];
    this.date = new Date(array[3]);
    this.level = level;
    this.levelName = getLevelName(level);
    this.levelPrior = getPriorName(level);
    this.verbosity = options.verbosity[this.levelPrior];
    const source = array[5];
    this.source = source && {
      file: source[0],
      row: source[1],
      col: source[2],
    };
    this.message = array[6];
    this.payload = array[7];

    // V1 compatibility
    this.cheeky = true;
    this.typeKey = null;
  }

  getVerboseObject() {
    if (this._verboseObject != null) { return this._verboseObject; }
    let obj = {};
    obj.date = this.date;
    if (this.meta != null) { obj.meta = this.meta; }
    if (this.name != null) { obj.name = this.name; }
    obj.level = this.level;
    if (this.source != null) { obj.source = {file: this.source[0], row: this.source[1], col: this.source[2]}; }
    if (this.message != null) { obj.message = this.message; }
    if (this.payload !== void 0) { obj.payload = this.payload; }
    return this._verboseObject = obj;
  }
}

/*===================================================== Exports  =====================================================*/

module.exports = MessageV0;
module.exports.SCHEMA_VERSION = SCHEMA_VERSION;
