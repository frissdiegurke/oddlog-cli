"use strict";

// This file exports all used properties; even if fallback function (identity) would be equivalent.

const chalk = require("chalk");

const VALUE = {_fallback: (text) => chalk.yellow(text)};

/*===================================================== Exports  =====================================================*/

exports.LEVEL = {
  silent: (text) => chalk.dim(text),
  trace: (text) => chalk.gray(text),
  debug: (text) => chalk.green(text),
  verbose: (text) => chalk.cyan(text),
  info: (text) => chalk.blue(text),
  warn: (text) => chalk.yellow(text),
  error: (text) => chalk.red(text),
  fatal: (text) => chalk.bgRed(text)
};

exports.LINE = {
  trace: (text) => chalk.bgWhite(" ") + " " + text,
  debug: (text) => chalk.bgGreen(" ") + " " + text,
  verbose: (text) => chalk.bgCyan(" ") + " " + text,
  info: (text) => chalk.bgBlue(" ") + " " + text,
  warn: (text) => chalk.bgYellow(" ") + " " + text,
  error: (text) => chalk.bgRed(" ") + " " + text,
  fatal: (text) => chalk.bgRed(" ") + " " + text,
  _fallback: (text) => "  " + text
};

exports.LINE_HEAD = {_fallback: (text) => text};
exports.SOURCE = {_fallback: (text) => text};
exports.TIME = {_fallback: (text) => chalk.inverse(text)};
exports.META = {_fallback: (text) => chalk.dim(text)};
exports.NAME = {_fallback: (text) => chalk.italic(text)};
exports.MESSAGE = {_fallback: (text) => chalk.bold(text)};
exports.NO_MESSAGE = {_fallback: (text) => chalk.bold(text)};
exports.NATIVE_INLINE = VALUE;
exports.NATIVE_MULTI_LINE = VALUE;
exports.ERROR_STACK = {_fallback: (text) => chalk.red(text)};
exports.OBJECT_KEY = {_fallback: (text) => chalk.magenta(text)};
exports.OBJECT_COLON = {};
exports.OBJECT_FS = {};
exports.OBJECT_BRACE = {_fallback: (text) => chalk.green(text)};

exports.TYPES = {
  NULL: VALUE,
  boolean: VALUE,
  string: VALUE,
  number: VALUE
};

exports.ARRAY_KEY = exports.ARRAY_INLINE_KEY = exports.OBJECT_INLINE_KEY = exports.OBJECT_KEY;
exports.ARRAY_COLON = exports.ARRAY_INLINE_COLON = exports.OBJECT_INLINE_COLON = exports.OBJECT_COLON;
exports.ARRAY_FS = exports.ARRAY_INLINE_FS = exports.OBJECT_INLINE_FS = exports.OBJECT_FS;
exports.ARRAY_BRACE = exports.ARRAY_INLINE_BRACE = exports.OBJECT_INLINE_BRACE = exports.OBJECT_BRACE;
