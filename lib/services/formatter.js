"use strict";

/*===================================================== Exports  =====================================================*/

exports.resolveFormatter = resolveFormatter;

/*==================================================== Functions  ====================================================*/

function resolveFormatter(format) {
  let mod = null;
  if (typeof format === "string") {
    mod = require("./formatter/" + format);
  } else if (typeof format === "object" && format !== null) {
    mod = format;
  }
  if (mod == null) { throw new Error("Unrecognized formatter option."); }
  if (!mod.hasOwnProperty("format")) { mod.format = identity; }
  if (!mod.hasOwnProperty("output")) { mod.output = outputString; }
  return mod;
}

function identity(value) { return value; }

function outputString(stream, string, options) { stream.write(string + options.EOL); }
