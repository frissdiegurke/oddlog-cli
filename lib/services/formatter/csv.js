"use strict";

// [RFC 4180](https://tools.ietf.org/html/rfc4180)

/*===================================================== Exports  =====================================================*/

exports.format = formatCSV;

/*==================================================== Functions  ====================================================*/

function formatCSV(message, options) {
  const FS = options.FS;
  if (FS.includes("\"")) { throw new Error("Field separator may not be '\"'."); }
  let metaEntry;
  if (message.isCustomMeta) {
    metaEntry = csvStringify(message.meta);
  } else {
    metaEntry = csvStringify(message.meta && [
        message.meta.platform,
        message.meta.host,
        message.meta.pid,
    ]);
  }
  const arr = [
    csvWrapString(message.version),
    csvWrapString(message.typeKey || ""),
    metaEntry,
    csvWrapString(message.name),
    csvWrapString(message.date.toISOString()),
    message.level,
    message.source && csvWrapString(message.source.file) || "",
    message.source && message.source.row || "",
    message.source && message.source.col || "",
    message.message && csvWrapString(message.message) || "",
    message.payload === void 0 ? "" : csvStringify(message.payload)
  ];
  return arr.join(FS);
}

function csvStringify(obj) { return csvWrapString(JSON.stringify(obj)); }

function csvWrapString(str) { return "\"" + str.toString().replace(/"/g, "\"\"") + "\""; }
