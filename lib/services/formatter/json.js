"use strict";

/*===================================================== Exports  =====================================================*/

exports.format = format;

/*==================================================== Functions  ====================================================*/

function format(message) { return JSON.stringify(message.getVerboseObject(), null, message.verbosity * 2 || null); }
