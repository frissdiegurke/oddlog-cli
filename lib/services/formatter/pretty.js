"use strict";

const {padLeft} = require("../padding");
const {getPadded: paddedLevelName} = require("../levels");
const {splitPayload, formatNative, getPayloadString} = require("../pretty-payload");

/*
 * Levels of verbosity:
 *
 * 0: [date] VERBOSE:  name  message
 * 1: [date] VERBOSE:  name  message
 *      optional file location
 *      optional error stack
 * 2: [date] VERBOSE:  name  message  inline-payload
 *      optional file location
 *      optional error stack
 * 3: [date] VERBOSE:  name  message  inline-payload
 *      optional file location
 *      optional error stack
 *      multi-line payload
 * 4: [date] VERBOSE/03:  name  message
 *      optional file location
 *      optional error stack
 *      payload
 * 5: [date] VERBOSE/03:  [optional meta data]  name  message
 *      optional file location
 *      optional error stack
 *      payload
 */

const MIN_VERBOSITY_ERROR_STACK = 1, MIN_VERBOSITY_LOCATION = 1;
const MIN_VERBOSITY_PAYLOAD_INLINE = 2, MIN_VERBOSITY_PAYLOAD_MULTI_LINE = 3, MIN_VERBOSITY_PAYLOAD_EXTRA = 4;
const MIN_VERBOSITY_LEVEL_NUM = 4;
const MIN_VERBOSITY_META = 5;

/*===================================================== Exports  =====================================================*/

exports.output = output;

/*==================================================== Functions  ====================================================*/

function output(stream, message, options) {
  let {payloadErrors, payloadInline, payloadMultiLine} =
      splitPayload(message, message.verbosity >= MIN_VERBOSITY_PAYLOAD_EXTRA);
  let result = options.style(getHeadline(message, options, payloadInline), message, options.STYLES.LINE_HEAD);
  if (message.verbosity >= MIN_VERBOSITY_LOCATION) {
    let str = getLocation(message, options);
    if (str !== void 0) { result += options.EOL + str; }
  }
  if (message.verbosity >= MIN_VERBOSITY_ERROR_STACK) {
    for (let i = 0; i < payloadErrors.length; i++) {
      let str = getErrorStack(message, options, payloadErrors[i]);
      if (str !== void 0) { result += options.EOL + str; }
    }
  }
  if (message.verbosity >= MIN_VERBOSITY_PAYLOAD_MULTI_LINE) {
    let str = getPayloadString(message, options, payloadMultiLine, 80);
    if (str !== void 0) { result += str; }
  }
  stream.write(result + options.EOL);
}

function getHeadline(message, options, payload) {
  let result = "";
  result += getDate(message, options) + " ";
  result += getLevel(message, options) + ":  ";
  if (message.verbosity >= MIN_VERBOSITY_META) {
    let str = getMeta(message, options);
    if (str !== void 0) { result += str + "  "; }
  }
  result += getName(message, options) + "  ";
  result += getText(message, options);
  if (message.verbosity >= MIN_VERBOSITY_PAYLOAD_INLINE && message.verbosity < MIN_VERBOSITY_PAYLOAD_EXTRA) {
    let str = getPayloadInline(message, options, payload);
    if (str !== void 0) { result += "  " + str; }
  }
  return result;
}

/*------------------------------------------------ simple properties  ------------------------------------------------*/

function getDate(message, options) {
  let result = "[", date = message.date;
  if (options.isoDate) {
    result += date.toISOString();
  } else {
    let hours = padLeft(date.getHours(), 0, 2), minutes = padLeft(date.getMinutes(), 0, 2),
        seconds = padLeft(date.getSeconds(), 0, 2), milliseconds = padLeft(date.getMilliseconds(), 0, 3);
    result += hours + ":" + minutes + ":" + seconds + "." + milliseconds;
  }
  result += "]";
  return options.style(result, message, options.STYLES.TIME);
}

function getLevel(message, options) {
  let result = paddedLevelName(message.level);
  if (message.verbosity >= MIN_VERBOSITY_LEVEL_NUM) { result += "/" + padLeft(message.level, 0, 2); }
  return options.style(result, message, options.STYLES.LEVEL);
}

function getMeta(message, options) {
  let meta = message.meta;
  if (meta != null) {
    let result = "[";
    if (meta.host) { result += meta.host; }
    if (meta.platform || meta.pid) { result += "/"; }
    if (meta.pid) { result += meta.pid + (meta.platform ? " " : ""); }
    if (meta.platform) { result += meta.platform; }
    result += "]";
    return options.style(result, message, options.STYLES.META);
  }
}

function getName(message, options) { return options.style(message.name, message, options.STYLES.NAME); }

function getText(message, options) {
  if (message.message == null) { return options.style("-", message, options.STYLES.NO_MESSAGE); }
  return options.style(message.message, message, options.STYLES.MESSAGE);
}

function getLocation(message, options) {
  let source = message.source;
  if (source != null) {
    const sourceArray = [source.file, source.row, source.col];
    let sourceStr = options.style(sourceArray.join(":"), message, options.STYLES.SOURCE);
    return options.style(sourceStr, message, options.STYLES.LINE);
  }
}

/*----------------------------------------------------- payload  -----------------------------------------------------*/

function getErrorStack(message, options, payload) {
  if (payload == null || typeof payload.stack !== "string") { return; }
  let result = "";
  let lines = payload.stack.split(/\r?\n\r?/);
  for (let i = 0; i < lines.length; i++) {
    let line = options.style(lines[i], message, options.STYLES.ERROR_STACK);
    if (i) { result += options.EOL; }
    result += options.style(line, message, options.STYLES.LINE);
  }
  return result;
}

function getPayloadInline(message, options, payload) {
  if (payload === void 0) { return; }
  if (typeof payload !== "object" || payload === null) {
    return options.style(formatNative(payload), message, options.STYLES.NATIVE_INLINE);
  }
  let result = "";
  if (Array.isArray(payload)) {
    result += options.style("[", message, options.STYLES.ARRAY_INLINE_BRACE);
    for (let i = 0; i < payload.length; i++) {
      if (i) { result += options.style(", ", message, options.STYLES.ARRAY_INLINE_FS); }
      result += options.style(i, message, options.STYLES.ARRAY_INLINE_KEY);
      result += options.style(":", message, options.STYLES.ARRAY_INLINE_COLON);
      result += options.styleNative(formatNative(payload[i]), message);
    }
    result += options.style("]", message, options.STYLES.ARRAY_INLINE_BRACE);
  } else {
    let first = true;
    result += options.style("{", message, options.STYLES.OBJECT_INLINE_BRACE);
    for (let key in payload) {
      if (!first) { result += options.style(", ", message, options.STYLES.OBJECT_INLINE_FS); }
      first = false;
      //noinspection JSUnfilteredForInLoop
      result += options.style(key, message, options.STYLES.OBJECT_INLINE_KEY);
      result += options.style(":", message, options.STYLES.OBJECT_INLINE_COLON);
      //noinspection JSUnfilteredForInLoop
      result += options.styleNative(formatNative(payload[key]), message);
    }
    result += options.style("}", message, options.STYLES.OBJECT_INLINE_BRACE);
  }
  return result;
}
