"use strict";

const {LIST, MAPPED, PADDED, MAX_LENGTH} = require("../constants/levels");
const {padLeft} = require("./padding");

/*===================================================== Exports  =====================================================*/

exports.getPadded = getPadded;
exports.getLevelName = getLevelName;
exports.getLevelValue = getLevelValue;
exports.getPriorName = getPriorName;

/*================================================ Initial Execution  ================================================*/

for (let i = 0; i < LIST.length; i++) {
  let padded = LIST[i].toUpperCase();
  while (padded.length < MAX_LENGTH) { padded = " " + padded; }
  MAPPED[LIST[i]] = i;
  PADDED[i] = padded;
}

/*==================================================== Functions  ====================================================*/

function getPadded(value) { return PADDED.hasOwnProperty(value) ? PADDED[value] : padLeft(value, " ", MAX_LENGTH); }

function getLevelValue(nameOrValue, fallback) {
  if (typeof nameOrValue === "string") {
    nameOrValue = nameOrValue.toLowerCase();
    if (MAPPED.hasOwnProperty(nameOrValue)) { return MAPPED[nameOrValue]; }
  } else if (typeof nameOrValue === "number") {
    return nameOrValue;
  }
  return fallback;
}

function getLevelName(value) {
  if (Number.isInteger(value) && value >= 0 && value < LIST.length) { return LIST[value]; }
  return value.toString();
}

function getPriorName(value) {
  if (Number.isInteger(value) && value >= 0 && value < LIST.length) { return LIST[value]; }
  if (typeof value !== "number" || value <= 0) { return LIST[0]; }
  if (value > LIST.length - 1) { return LIST[LIST.length - 1]; }
  return LIST[value | 0];
}
