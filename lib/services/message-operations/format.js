"use strict";

/*===================================================== Exports  =====================================================*/

module.exports = format;

/*==================================================== Functions  ====================================================*/

function format(message, options) { return options.formatter.format(message, options); }
