"use strict";

/*===================================================== Exports  =====================================================*/

module.exports = output;

/*==================================================== Functions  ====================================================*/

function output(stream, message, options) { return options.formatter.output(stream, message, options); }
