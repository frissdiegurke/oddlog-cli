"use strict";

/*===================================================== Exports  =====================================================*/

module.exports = show;

/*==================================================== Functions  ====================================================*/

function show(message, options) {
  const filter = options.filter;
  if (filter.level != null && message.level < filter.level) { return false; }
  if (filter.date != null && !filter.date.matches(message)) { return false; }
  if (filter.name != null && !filter.name.test(message.name)) { return false; }
  //
  return true;
}
