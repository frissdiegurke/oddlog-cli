"use strict";

const MessageV0 = require("../classes/MessageV0");
const MessageV1 = require("../classes/MessageV1");
const MessageV2 = require("../classes/MessageV2");
const MessageV3 = require("../classes/MessageV3");

const MAX_SCHEMA_VERSION = MessageV3.SCHEMA_VERSION;

/*===================================================== Exports  =====================================================*/

exports.create = createMessage;

/*==================================================== Functions  ====================================================*/

function createMessage(array, options) {
  const version = array[0];
  if (typeof version !== "number" || version < 0 || !Number.isInteger(version)) {
    throw new TypeError("Message object does not provide valid version information.");
  }
  if (version > MAX_SCHEMA_VERSION) {
    throw new Error("Log data version " + version + " not supported. You might just need to update oddlog-cli.");
  }

  if (version === 0) {
    return new MessageV0(version, array, options);
  } else if (version === 1) {
    return new MessageV1(version, array, options);
  } else if (version === 2) {
    return new MessageV2(version, array, options);
  } else if (version === 3) {
    return new MessageV3(version, array, options);
  } else {
    throw new Error("Log data version " + version + " invalid.");
  }
}
