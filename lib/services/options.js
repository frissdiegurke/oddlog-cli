"use strict";

const nsMatcher = require("ns-matcher");
const path = require("path");

const {LIST: LEVELS} = require("../constants/levels");
const {resolveStyles, styleNative, style} = require("./styles");
const {resolveFormatter} = require("./formatter");
const {getLevelValue} = require("./levels");
const {getDateFilter} = require("./date-parser");

/*===================================================== Exports  =====================================================*/

exports.fromArguments = fromArguments;

/*==================================================== Functions  ====================================================*/

function fromArguments(args) {
  let userModule = getUserModule(args);
  return {
    FS: args.FS,
    EOL: args.EOL,
    STYLES: getStyles(args, userModule),

    userModule,
    verbosity: getVerbosity(args),
    newline: !!args.newline,
    encoding: args.encoding,
    isoDate: !!args.isoDate,
    showInvalid: !!args.invalid,
    formatter: resolveFormatter(args.format),

    filter: {
      level: getLevel(args.level),
      name: getNameFilter(args.name),
      date: getDateFilter(args.date)
    },

    style,
    styleNative(...args) { return Reflect.apply(styleNative, null, [this.STYLES].concat(args)); }
  };
}

function getUserModule(args) {
  let modString = args.module;
  if (typeof modString !== "string") { return null; }
  try {
    return require(modString[0] === "/" || modString[0] === "." ? path.join(process.cwd(), modString) : modString);
  } catch (e) {
    return null;
  }
}

function getLevel(level) {
  let lvl = +(level == null ? null : level.trim());
  level = level == null ? -Infinity : getLevelValue(Number.isNaN(lvl) ? level : lvl, -Infinity);
  return level;
}

function getNameFilter(array) {
  let patterns = array == null || !array.length ? process.env.DEBUG || null : array.join(",");
  return patterns && nsMatcher(patterns);
}

function getStyles(args, userModule) {
  let styles = (args.style == null || args.style === "module")
      ? (userModule && userModule.hasOwnProperty("style") ? userModule.style : "default")
      : args.style;
  return resolveStyles(styles);
}

function getVerbosity(args) {
  let result = {};
  let verbosity = args.verbose || 0;

  for (let i = 0; i < LEVELS.length; i++) {
    let levelName = LEVELS[i], v = args["verbosity-" + levelName];
    if (typeof v === "number" && !Number.isNaN(v) && v >= 0) { verbosity = Math.ceil(v); }
    result[levelName] = verbosity;
  }

  return result;
}
