"use strict";

/*===================================================== Exports  =====================================================*/

exports.resolveStyles = resolveStyles;
exports.styleNative = styleNative;
exports.style = style;

/*==================================================== Functions  ====================================================*/

function resolveStyles(style) {
  let mod = null;
  if (typeof style === "string") {
    mod = require("../constants/styles/" + style);
  } else if (typeof style === "object" && style !== null) {
    mod = style;
  }
  if (mod == null) { throw new Error("Unrecognized style option."); }
  if (!mod.hasOwnProperty("TYPES")) { mod.TYPES = {}; }
  return mod;
}

function style(text, message, styleObj) {
  if (styleObj == null) { return text; }
  if (styleObj.hasOwnProperty(message.levelName)) { return styleObj[message.levelName](text, message); }
  if (styleObj.hasOwnProperty("_fallback")) { return styleObj._fallback(text, message); }
  return text;
}

function styleNative(styles, text, message) {
  let type = typeof text;
  let types = styles.TYPES || {};
  if (type === "object" && text !== null) { throw new Error("Objects ain't native value"); }
  return style(text, message, types[text === null ? "NULL" : type]);
}
